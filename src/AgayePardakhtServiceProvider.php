<?php

namespace MahanShoghy\LaravelAgayePardakht;

use Illuminate\Support\ServiceProvider;

class AgayePardakhtServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__.'/config.php', 'agaye-pardakht');
    }

    public function boot(): void
    {
        if ($this->app->runningInConsole()) {

            $this->publishes([
                __DIR__.'/config.php' => config_path('agaye-pardakht.php')
            ], 'config');
        }
    }
}
