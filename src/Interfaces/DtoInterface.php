<?php

namespace MahanShoghy\LaravelAgayePardakht\Interfaces;

interface DtoInterface
{
    public function __construct(array $data);

    public function get(): mixed;
}
