<?php

namespace MahanShoghy\LaravelAgayePardakht\App;

class ApiProvider
{
    protected const CREATE_TRANSACTION_ENDPOINT = 'api/v2/create';
    protected const CREATE_TRANSACTION_SANDBOX_ENDPOINT = 'api/v2/sandbox/create';
    protected const START_PAY_ENDPOINT = 'startpay/:transid';
    protected const START_PAY_SANDBOX_ENDPOINT = 'startpay/sandbox/:transid';
    protected const VERIFY_ENDPOINT = 'api/v2/verify';
    protected const VERIFY_SANDBOX_ENDPOINT = 'api/v2/sandbox/verify';

    protected static function getHost(): ?string
    {
        return config('agaye-pardakht.host');
    }

    protected static function getPin(): ?string
    {
        return (self::isSandbox()) ? 'sandbox': config('agaye-pardakht.pin');
    }

    protected static function isSandbox(): bool
    {
        return config('agaye-pardakht.sandbox');
    }
}
