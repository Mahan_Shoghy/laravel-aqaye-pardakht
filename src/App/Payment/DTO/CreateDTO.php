<?php

namespace MahanShoghy\LaravelAgayePardakht\App\Payment\DTO;

use MahanShoghy\LaravelAgayePardakht\App\Payment\Enums\CreateStatusEnum;
use MahanShoghy\LaravelAgayePardakht\App\Payment\Objects\CreateObjectClass;
use MahanShoghy\LaravelAgayePardakht\Interfaces\DtoInterface;

class CreateDTO implements DtoInterface
{
    private CreateObjectClass $object;

    public function __construct(array $data)
    {
        $this->object = new CreateObjectClass(
            CreateStatusEnum::getFrom($data['status']),
            $data['transid']
        );
    }

    public function get(): CreateObjectClass
    {
        return $this->object;
    }
}
