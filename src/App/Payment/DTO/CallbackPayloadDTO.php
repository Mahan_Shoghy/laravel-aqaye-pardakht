<?php

namespace MahanShoghy\LaravelAgayePardakht\App\Payment\DTO;

use Illuminate\Http\Request;
use MahanShoghy\LaravelAgayePardakht\App\Payment\Enums\CallbackStatusEnum;
use MahanShoghy\LaravelAgayePardakht\App\Payment\Objects\CallbackPayload;

class CallbackPayloadDTO
{
    private CallbackPayload $payload;

    public function __construct(Request $request)
    {
        $this->payload = new CallbackPayload(
            $request->get('transid'),
            CallbackStatusEnum::getFrom((int) $request->get('status')),
            $request->get('cardnumber'),
            $request->get('tracking_number'),
            $request->get('invoice_id'),
            $request->get('bank')
        );
    }

    public function get(): CallbackPayload
    {
        return $this->payload;
    }
}
