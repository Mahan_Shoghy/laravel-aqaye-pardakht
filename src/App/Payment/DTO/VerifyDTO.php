<?php

namespace MahanShoghy\LaravelAgayePardakht\App\Payment\DTO;

use MahanShoghy\LaravelAgayePardakht\App\Payment\Objects\VerifyObject;
use MahanShoghy\LaravelAgayePardakht\Interfaces\DtoInterface;

class VerifyDTO implements DtoInterface
{
    private VerifyObject $object;

    public function __construct(array $data)
    {
        $this->object = new VerifyObject(
            $data['status'],
            $data['code']
        );
    }

    public function get(): VerifyObject
    {
        return $this->object;
    }
}
