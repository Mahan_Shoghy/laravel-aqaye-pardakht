<?php

namespace MahanShoghy\LaravelAgayePardakht\App\Payment\Enums;

use MahanShoghy\PhpEnumHelper\EnumHelper;

enum CallbackStatusEnum: int
{
    use EnumHelper;

    case SUCCESS = 1;
    case ERROR = 0;
}
