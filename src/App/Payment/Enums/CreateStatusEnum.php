<?php

namespace MahanShoghy\LaravelAgayePardakht\App\Payment\Enums;

use MahanShoghy\PhpEnumHelper\EnumHelper;

enum CreateStatusEnum: string
{
    use EnumHelper;

    case SUCCESS = 'success';
    case ERROR = 'error';
}
