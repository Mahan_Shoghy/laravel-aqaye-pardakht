<?php

namespace MahanShoghy\LaravelAgayePardakht\App\Payment;

use Exception;
use Throwable;

class PaymentException extends Exception
{
    private int $response_code;

    public function __construct(array $response, int $code = 422, ?Throwable $previous = null)
    {
        $this->response_code = $response['code'];

        parent::__construct($this->getResponseDescription(), $code, $previous);
    }

    public function getResponseCode(): int
    {
        return $this->response_code;
    }

    public function getResponseDescription(): string
    {
        return match ($this->getResponseCode()) {
            2 => 'تراکنش قبلا وریفای و پرداخت شده است',
            -1 => 'amount نمی تواند خالی باشد',
            -2 => 'کد پین درگاه نمی تواند خالی باشد',
            -3 => 'callback نمی تواند خالی باشد',
            -4 => 'amount باید عددی باشد',
            -5 => 'amount باید بین 1,000 تا 100,000,000 تومان باشد',
            -6 => 'کد پین درگاه اشتباه هست',
            -7 => 'transid نمی تواند خالی باشد',
            -8 => 'تراکنش مورد نظر وجود ندارد',
            -9 => 'کد پین درگاه با درگاه تراکنش مطابقت ندارد',
            -10 => 'مبلغ با مبلغ تراکنش مطابقت ندارد',
            -11 => 'درگاه درانتظار تایید و یا غیر فعال است',
            -12 => 'امکان ارسال درخواست برای این پذیرنده وجود ندارد',
            -13 => 'شماره کارت باید 16 رقم چسبیده بهم باشد',
            -14 => 'درگاه برروی سایت دیگری درحال استفاده است'
        };
    }
}
