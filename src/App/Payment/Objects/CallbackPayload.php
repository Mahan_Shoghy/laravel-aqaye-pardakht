<?php

namespace MahanShoghy\LaravelAgayePardakht\App\Payment\Objects;

use MahanShoghy\LaravelAgayePardakht\App\Payment\Enums\CallbackStatusEnum;
use MahanShoghy\LaravelAgayePardakht\ObjectClass;

class CallbackPayload extends ObjectClass
{
    /**
     * @param string $transid کد تراکنش
     * @param CallbackStatusEnum $status وضعیت تراکنش.
     * @param string|null $card_number شماره کارت پرداخت کننده که درصورت موفق بودن تراکنش دریافت میکنید.
     * @param string|null $tracking_number شماره تراکنش
     * @param string|null $invoice_id شماره فاکتور ارسال شده در هنگام درخواست ایجاد تراکنش (در صورت ارسال)
     * @param string|null $bank نام بانک پرداخت
     */
    public function __construct(
        public readonly string $transid,
        public readonly CallbackStatusEnum $status,
        public readonly ?string $card_number,
        public readonly ?string $tracking_number,
        public readonly ?string $invoice_id,
        public readonly ?string $bank
    ){}
}
