<?php

namespace MahanShoghy\LaravelAgayePardakht\App\Payment\Objects;

use MahanShoghy\LaravelAgayePardakht\App\Payment\Enums\CreateStatusEnum;
use MahanShoghy\LaravelAgayePardakht\ObjectClass;

class CreateObjectClass extends ObjectClass
{
    public function __construct(
        public readonly CreateStatusEnum $status,
        public readonly string           $transid,
    ){}
}
