<?php

namespace MahanShoghy\LaravelAgayePardakht\App\Payment\Objects;

use MahanShoghy\LaravelAgayePardakht\ObjectClass;

class VerifyObject extends ObjectClass
{
    public function __construct(
        public readonly string $status,
        public readonly string $code
    ){}
}
