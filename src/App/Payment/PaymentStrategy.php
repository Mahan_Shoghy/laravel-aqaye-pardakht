<?php

namespace MahanShoghy\LaravelAgayePardakht\App\Payment;

use MahanShoghy\LaravelAgayePardakht\App\Payment\PaymentException;
use MahanShoghy\LaravelAgayePardakht\App\Payment\Objects\CallbackPayload;
use MahanShoghy\LaravelAgayePardakht\App\Payment\Objects\CreateObjectClass;
use MahanShoghy\LaravelAgayePardakht\App\Payment\Objects\VerifyObject;

interface PaymentStrategy
{
    /**
     * Create Transaction
     *
     * @param int $amount مبلغ تراکنش [تومان] بین 1,000 تا 100,000,000 تومان
     * @param string $callback آدرس برگشت [نتیجه پرداخت به این آدرس ارسال می شود]
     * @param string|null $card_number شماره کارت مجاز به پرداخت
     * @param string|null $invoice_id شماره فاکتور
     * @param string|null $mobile ذخیره شماره کارت در درگاه برای پرداخت های بعدی
     * @param string|null $email ایمیل پرداخت کننده
     * @param string|null $description توضیحات
     * @return CreateObjectClass
     * @throws PaymentException
     */
    public static function create(int $amount, string $callback, ?string $card_number = null, ?string $invoice_id = null, ?string $mobile = null, ?string $email = null, ?string $description = null): CreateObjectClass;

    /**
     * Get start pay url
     *
     * @param string $transid کد تراکنش
     * @return string
     */
    public static function getPayUrl(string $transid): string;

    /**
     * Verify Transaction
     *
     * @param CallbackPayload $payload
     * @param int $amount
     * @return VerifyObject
     * @throws PaymentException
     */
    public static function verify(CallbackPayload $payload, int $amount): VerifyObject;
}
