<?php

namespace MahanShoghy\LaravelAgayePardakht\App\Payment;

use Illuminate\Support\Facades\Http;
use MahanShoghy\LaravelAgayePardakht\App\ApiProvider;
use MahanShoghy\LaravelAgayePardakht\App\Payment\DTO\CreateDTO;
use MahanShoghy\LaravelAgayePardakht\App\Payment\DTO\VerifyDTO;
use MahanShoghy\LaravelAgayePardakht\App\Payment\Objects\CallbackPayload;
use MahanShoghy\LaravelAgayePardakht\App\Payment\Objects\CreateObjectClass;
use MahanShoghy\LaravelAgayePardakht\App\Payment\Objects\VerifyObject;

class Payment extends ApiProvider implements PaymentStrategy
{
    public static function create(int $amount, string $callback, ?string $card_number = null, ?string $invoice_id = null, ?string $mobile = null, ?string $email = null, ?string $description = null): CreateObjectClass
    {
        $data = array_merge(['pin' => self::getPin()], compact([
            'amount',
            'callback',
        ]));
        if ($card_number){
            $data['card_number'] = $card_number;
        }
        if ($invoice_id){
            $data['invoice_id'] = $invoice_id;
        }
        if ($mobile){
            $data['mobile'] = $mobile;
        }
        if ($email){
            $data['email'] = $email;
        }
        if ($description){
            $data['description'] = $description;
        }

        $response = Http::post(self::getHost().'/'.self::CREATE_TRANSACTION_ENDPOINT, $data);
        $response = $response->json();

        if ($response['status'] === 'error'){
            throw new PaymentException($response);
        }

        return (new CreateDTO($response))->get();
    }

    public static function getPayUrl(string $transid): string
    {
        $url = self::getHost().'/';
        $url .= (self::isSandbox())
            ? self::START_PAY_SANDBOX_ENDPOINT
            : self::START_PAY_ENDPOINT;

        return str($url)->replace(':transid', $transid);
    }

    public static function verify(CallbackPayload $payload, int $amount): VerifyObject
    {
        $data = array_merge(['pin' => self::getPin()], [
            'transid' => $payload->transid,
            'amount' => $amount,
        ]);

        $response = Http::post(self::getHost().'/'.self::VERIFY_ENDPOINT, $data);
        $response = $response->json();

        if ($response['status'] === 'error' && $response['code'] !== '0'){
            throw new PaymentException($response);
        }

        return (new VerifyDTO($response))->get();
    }
}
