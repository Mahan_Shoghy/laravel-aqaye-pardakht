<?php

namespace MahanShoghy\LaravelAgayePardakht;

class ObjectClass
{
    public function toArray(): array
    {
        $vars = get_object_vars($this);

        $array = [];
        foreach ($vars as $key => $value){
            $array[$key] = $value;
        }

        return $array;
    }
}
