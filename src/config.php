<?php

return [
    'host' => env('AGAYE_PARDAKHT_HOST', 'https://panel.aqayepardakht.ir'),

    'pin' => env('AGAYE_PARDAKHT_PIN'),

    'sandbox' => env('AGAYE_PARDAKHT_SANDBOX', env('APP_ENV', 'local') !== 'production')
];
